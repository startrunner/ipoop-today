﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IPoopToday.Web.Data;
using IPoopToday.Web.Services;
using IPoopToday.Web.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using IPoopToday.Web.Infrastructure;
using Microsoft.AspNetCore.Rewrite;

namespace IPoopToday.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"connectionString.json", optional: false);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IHealthService, HealthService>();
                
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<Shitee, IdentityRole>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                options.SecurityStampValidationInterval = TimeSpan.FromSeconds(15);

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // Cookie settings
                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);
                options.Cookies.ApplicationCookie.LoginPath = "/Account/LogIn";
                options.Cookies.ApplicationCookie.LogoutPath = "/Account/LogOff";
            });

            services.AddMvc(config =>
            {

            });

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider svcProvider)
        {

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseStatusCodePages(async context =>
                {
                    await Task.Yield();
                    context
                        .HttpContext
                        .Response
                        .Redirect(
                            $"/Home/Error/{context.HttpContext.Response.StatusCode}"
                        );
                });

                /*
                app.UseRewriter(
                    new RewriteOptions()
                        .Add(new DomainRewriterRule())
                        
                );
                */
            }

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "censoring",
                    template: "Restroom/{action=Index}/{id?}",
                    defaults: new {controller = "Shitter"});
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Shitter}/{action=Index}/{id?}");
            });

            SeedDatabase(svcProvider);
        }

        public async void SeedDatabase(IServiceProvider svcProvider)
        {
            var userManager = svcProvider.GetRequiredService<UserManager<Shitee>>();
            var db = svcProvider.GetRequiredService<ApplicationDbContext>();

            await CreateUser(userManager);
            await CreateShitters(db);
        }

        private static async System.Threading.Tasks.Task CreateUser(UserManager<Shitee> userManager)
        {
            var email = "gosho@gmail.com";
            var admin = new Shitee { Email = email, UserName = email, EmailConfirmed = true };
            await userManager.CreateAsync(admin, "1234");
        }

        private async System.Threading.Tasks.Task CreateShitters(ApplicationDbContext db)
        {
            if (await db.Shitters.AnyAsync()) return;
            var author = await db.Users.FirstAsync();
            var shitters = new List<Shitter>
            {
                new Shitter
                {
                    Location = new Geolocation(5, 10),
                    Name = "Shitter1",
                    Author=author
                },
                new Shitter
                {
                    Location = new Geolocation(25, 22),
                    Name = "Shitter2",
                    Author=author
                },
                new Shitter
                {
                    Location = new Geolocation(15, 2),
                    Name = "Shitter3",
                    Author =author                },
            };
            await db.Shitters.AddRangeAsync(shitters);
            await db.SaveChangesAsync();
        }
    }
}
