﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Services
{
    public interface IHealthService
    {
        bool ShouldDelete(int yesVotes, int noVotes);
        double CalculateHealthPercentage(int yesVotes, int noVotes);
        Task<HealthServiceResult> CheckAndDeleteAsync(Guid shitterID);
    }
}
