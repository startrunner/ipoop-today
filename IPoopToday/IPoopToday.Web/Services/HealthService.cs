﻿using IPoopToday.Web.Data;
using IPoopToday.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Services
{
    public class HealthService : IHealthService
    {

        private const double MinimumDifferenceCoefficient = 0.75;

        private ApplicationDbContext _db;

        public HealthService(ApplicationDbContext db)
        {
            _db = db;
        }

        public bool ShouldDelete(int yesVotes, int noVotes)
        {
            if (noVotes + yesVotes < 10) return false;
            if (noVotes < yesVotes) return false;
            double minDifference = yesVotes * MinimumDifferenceCoefficient;
            int minNoVotes = (int)Math.Ceiling(yesVotes + minDifference);
            return noVotes >= minNoVotes;
        }

        public double CalculateHealthPercentage(int yesVotes, int noVotes)
        {
            return (double) yesVotes / noVotes;
        }

        public async Task<HealthServiceResult> CheckAndDeleteAsync(Guid shitterID)
        {
            HealthServiceResult result = new HealthServiceResult() {
                Deleted = false
            };

            Shitter shitter = _db
                .Shitters
                .Where(x => x.ID == shitterID)
                .SingleOrDefault();

            if (shitter == null)
            {
                return result;
            }

            int votesTotal = shitter.Votes.Count;
            int votesYes = shitter.Votes.Where(x => x.IsPositive).Count();

            if(ShouldDelete(votesYes, votesTotal-votesYes))
            {
                _db.Shitters.Remove(shitter);
                await _db.SaveChangesAsync();
                result.Deleted = true;
            }
            return result;
        }
    }
}
