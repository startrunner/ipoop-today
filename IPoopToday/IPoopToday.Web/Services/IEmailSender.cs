﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPoopToday.Web.Entities;
using Microsoft.AspNetCore.Mvc;

namespace IPoopToday.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
        Task SendConfirmationEmailAsync(Shitee user, IUrlHelper urlHelper);
    }
}
