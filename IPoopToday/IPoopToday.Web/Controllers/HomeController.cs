﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace IPoopToday.Web.Controllers
{
    public class HomeController : Controller
    {
        static readonly IReadOnlyList<int> AvailableErrorPages = new int[] {
            0, 400, 401, 403, 404, 500, 501, 502, 503, 520, 521, 533
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error(int id)
        {
            //id is errorCode

            if(AvailableErrorPages.Contains(id))
            {
                return PartialView($"ErrorPages/HTTP{id}");
            }
            else
            {
                return Content($"Http Error {id}... :/");
            }
        }
    }
}
