using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPoopToday.Web.Models.ShitterViewModels;
using Microsoft.AspNetCore.Mvc;
using IPoopToday.Web.Data;
using IPoopToday.Web.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IPoopToday.Web.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using IPoopToday.Web.Services;

namespace IPoopToday.Web.Controllers
{
    [Authorize]
    public class ShitterController : Controller
    {
        private readonly ApplicationDbContext _db;

        private readonly UserManager<Shitee> _userManager;
        private IHealthService _healthService;

        public ShitterController(
            ApplicationDbContext db,
            UserManager<Shitee> userManager,
            IHealthService healthService)
        {
            _healthService = healthService;
            _userManager = userManager;
            _db = db;
        }

        private string UserID=>
            _userManager.GetUserId(User);

        [HttpGet]
        public ActionResult List()
        {
            var shitters = GetShittersForCurrentUser(true);
            var viewModels = shitters.Select(CreateViewModel).ToArray();
            return View(viewModels);
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(nameof(Map));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Details(Guid id)
        {
            var shitter = GetShitterById(id);
            if (shitter == null) return NotFound();

            var shitterViewModel = CreateViewModel(shitter);

            return View(shitterViewModel);
        }

        #region Create

        [HttpGet]
        public ActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ShitterViewModel vm)
        {
            if (!ModelState.IsValid) return View(vm);
            ShitterTagsViewModel tags = Request.Form.GetShitterTags();

            var shitter = new Shitter() {
                Author = await _userManager.GetUserAsync(User),
                Location = new Geolocation(vm.Latitude, vm.Longitude),
                Name = vm.Name,
                Tags = (int)tags
            };

            _db.Shitters.Add(shitter);
            _db.SaveChanges();

            return RedirectToAction(nameof(Details), new {id = shitter.ID});
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            Shitter shitter = GetShitterByIdWithAccessCheck(id);
            if (shitter == null) return NotFound();
            var vm = CreateViewModel(shitter);

            return View("Create", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ShitterViewModel vm)
        {
            if (!ModelState.IsValid) return View("Create", vm);

            Shitter shitter = GetShitterByIdWithAccessCheck(vm.ID);
            if (shitter == null) return NotFound();

            shitter.Location = new Geolocation(vm.Latitude, vm.Longitude);
            shitter.Name = vm.Name;
            shitter.Tags = (int)Request.Form.GetShitterTags();

            _db.SaveChanges();

            return RedirectToAction(nameof(Details), new {id = vm.ID});
        }

        #endregion

        #region Map

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Map() => View();

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAll()
        {
            return Json(GetAllShitters()
                .Select(CreateViewModel));
        }

        #endregion

        #region Delete

        [HttpGet]
        public ActionResult ConfirmDelete(Guid id)
        {
            var shitter = GetShitterByIdWithAccessCheck(id);
            if (shitter == null) return NotFound();
            return View(new ShitterViewModel(shitter.ID, shitter.Name,
                shitter.Location.Longitude, shitter.Location.Latitude));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete([FromForm]Guid id)
        {
            try
            {
                var shitter = GetShitterByIdWithAccessCheck(id);
                _db.Shitters.Remove(shitter);
                _db.SaveChanges();
                return RedirectToAction(nameof(List));
            }
            catch
            {
                return RedirectToAction(nameof(ConfirmDelete), new {id = id});
            }
        }

        #endregion

        #region Vote

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Vote(ShitterVoteViewModel voteViewModel)
        {
            if (!ModelState.IsValid) return BadRequest();
            Shitter shitter = GetShitterById(voteViewModel.ShitterID);
            if (shitter == null) return NotFound();

            Shitee user = await _userManager.GetUserAsync(User);
            var vote = GetVoteForCurrentUser(voteViewModel.ShitterID);
            if (vote != null)
            {
                if (vote.IsPositive == voteViewModel.IsPositive)
                {
                    return await
                        InvokeHealthServiceAndReturn(
                            shitter.ID,
                            NoContent()
                        );
                }
                vote.IsPositive = voteViewModel.IsPositive;
            }
            else
            {
                vote = new Vote {
                    Shitter = shitter,
                    User = user,
                    Time = DateTime.Now,
                    IsPositive = voteViewModel.IsPositive
                };
                _db.Votes.Add(vote);
            }
            await _db.SaveChangesAsync();

            return await 
                InvokeHealthServiceAndReturn(
                    shitter.ID, Json(voteViewModel.IsPositive)
                );
        }

        #endregion

        private IList<Shitter> GetShittersForCurrentUser(bool loadReviews=false)
        {
            var query = GetAllShitters()
                .Where(x => x.Author.Id == UserID);

            if (loadReviews) query = query.Include(x => x.Reviews);

            return query.ToList();
        }

        private Shitter GetShitterByIdWithAccessCheck(Guid id)
        {
            var shitter = GetShitterById(id);
            if (!CheckShitterAccess(shitter)) return null;
            return shitter;
        }

        private Shitter GetShitterById(Guid id)
        {
            var rt= GetAllShitters()
                .Include(x=>x.Reviews)
                .SingleOrDefault(x => x.ID == id);

            return rt; 
        }

        private IQueryable<Shitter> GetAllShitters()
        {
            return _db.Shitters
                .Include(x => x.Location)
                .Include(x => x.Reviews)
                .Include(x => x.Author)
                .Include(x => x.Votes);
        }

        private bool CheckShitterAccess(Shitter shitter)
        {
            if (shitter?.Author?.Id != UserID) return false;
            return true;
        }

        private ShitterViewModel CreateViewModel(Shitter shitter)
        {
            var result = new ShitterViewModel {
                ID = shitter.ID,
                Latitude = shitter.Location.Latitude,
                Longitude = shitter.Location.Longitude,
                Name = shitter.Name,
                Tags = (ShitterTagsViewModel)shitter.Tags,
                VotesYes = shitter.VotesYes,
                VotesNo = shitter.VotesNo,
                IsPositiveVote = GetVoteForCurrentUser(shitter.ID)?.IsPositive,
                AuthorID = shitter.Author.Id
            };
            if (shitter.Reviews?.Count > 0)
            {
                result.AverageRating = shitter.Reviews.Average(x => x.Rating);
            }
            return result;
        }

        private async Task<IActionResult> InvokeHealthServiceAndReturn(Guid shitterID, IActionResult result)
        {
            HealthServiceResult serviceResult= await 
                _healthService.CheckAndDeleteAsync(shitterID);

            if(serviceResult.Deleted)
            {
                return RedirectToAction(nameof(Map));
            }

            return result;
        }

        private Vote GetVoteForCurrentUser(Guid shitterId)
        {
            var rt= _db.Votes
                .Where(x => x.ShitterID == shitterId)
                .SingleOrDefault(x => x.UserID == UserID);
            return rt;
        }
    }
}