using System.Linq;
using System.Threading.Tasks;
using IPoopToday.Web.Data;
using IPoopToday.Web.Entities;
using IPoopToday.Web.Models.ReviewViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPoopToday.Web.Controllers
{
    [Authorize]
    public class ReviewController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Shitee> _userManager;
        public ReviewController(ApplicationDbContext context, UserManager<Shitee> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(ReviewViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest();

            var shitter = _context.Shitters
                                  .Include(x => x.Reviews)
                                  .FirstOrDefault(x => x.ID == model.ShitterID);
            if (shitter == null)
            {
                return NotFound();
            }

            shitter.Reviews.Add(new Review
            {
                Author = await _userManager.GetUserAsync(User),
                Content = model.Content,
                Rating = (byte)model.Rating
            });

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(ShitterController.Details), "Shitter", new {id = model.ShitterID});
        }

    }
}