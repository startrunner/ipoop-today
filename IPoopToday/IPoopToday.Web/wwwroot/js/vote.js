﻿$(document).ready(() => {
    updateVotes();
});

function vote(isPositive) {
    $.ajax({
        url: voteUrl,
        type: 'POST',
        data: {
            "shitterId": shitterId,
            "isPositive": isPositive
        },
        success: voteCallback
    });
}

function voteCallback(isPositive) {
    positiveVote = isPositive;

    if (isPositive) {
        yesVotes++;
        noVotes--;
    } else {
        yesVotes--;
        noVotes++;
    }

    if (noVotes < 0) noVotes = 0;
    if (yesVotes < 0) yesVotes = 0;

    updateVotes();
}

function updateVotes() {
    updateVoteButtons();
    updateVoteCounts();
}

function updateVoteCounts() {
    $("#yes-votes").html(yesVotes);
    $("#no-votes").html(noVotes);
}

function updateVoteButtons() {
    if (positiveVote === null) return;
    $("#btn-vote-yes").prop("disabled", positiveVote);
    $("#btn-vote-no").prop("disabled", !positiveVote);
}