﻿let currentRating = 2;

$('#span-stars input').click(e => {
    let target = $(e.target);
    currentRating = target.val();
    $('#input-rating').val(currentRating);
});