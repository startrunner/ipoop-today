﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPoopToday.Web.Data.Migrations
{
    public partial class timestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VotesNo",
                table: "Shitters");

            migrationBuilder.DropColumn(
                name: "VotesYes",
                table: "Shitters");

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Votes",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Shitters",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Geolocation",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "ActivityTypes",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Activities",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Shitters");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Geolocation");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "ActivityTypes");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Activities");

            migrationBuilder.AddColumn<int>(
                name: "VotesNo",
                table: "Shitters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VotesYes",
                table: "Shitters",
                nullable: false,
                defaultValue: 0);
        }
    }
}
