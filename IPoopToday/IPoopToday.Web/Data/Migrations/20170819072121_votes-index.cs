﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPoopToday.Web.Data.Migrations
{
    public partial class votesindex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Votes_UserID",
                table: "Votes");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_UserID_ShitterID",
                table: "Votes",
                columns: new[] { "UserID", "ShitterID" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Votes_UserID_ShitterID",
                table: "Votes");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_UserID",
                table: "Votes",
                column: "UserID");
        }
    }
}
