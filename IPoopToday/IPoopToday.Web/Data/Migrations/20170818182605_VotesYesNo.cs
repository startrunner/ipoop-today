﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPoopToday.Web.Data.Migrations
{
    public partial class VotesYesNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters");

            migrationBuilder.DropForeignKey(
                name: "FK_Shitters_Geolocation_LocationID",
                table: "Shitters");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Shitters",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LocationID",
                table: "Shitters",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "Shitters",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VotesNo",
                table: "Shitters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VotesYes",
                table: "Shitters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shitters_Geolocation_LocationID",
                table: "Shitters",
                column: "LocationID",
                principalTable: "Geolocation",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters");

            migrationBuilder.DropForeignKey(
                name: "FK_Shitters_Geolocation_LocationID",
                table: "Shitters");

            migrationBuilder.DropColumn(
                name: "VotesNo",
                table: "Shitters");

            migrationBuilder.DropColumn(
                name: "VotesYes",
                table: "Shitters");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Shitters",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "LocationID",
                table: "Shitters",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "Shitters",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shitters_Geolocation_LocationID",
                table: "Shitters",
                column: "LocationID",
                principalTable: "Geolocation",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
