﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IPoopToday.Web.Data.Migrations
{
    public partial class shitterauthor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        { 
            migrationBuilder.AddColumn<string>(
                name: "AuthorId",
                table: "Shitters",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Shitters_AuthorId",
                table: "Shitters",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shitters_AspNetUsers_AuthorId",
                table: "Shitters");

            migrationBuilder.DropIndex(
                name: "IX_Shitters_AuthorId",
                table: "Shitters");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Shitters");
        }
    }
}
