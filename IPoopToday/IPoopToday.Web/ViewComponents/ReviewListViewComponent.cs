﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPoopToday.Web.Data;
using IPoopToday.Web.Models.ReviewViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IPoopToday.Web.ViewComponents
{
    public class ReviewListViewComponent : ViewComponent
    { 
        private ApplicationDbContext _db;
        public ReviewListViewComponent(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid shitterId)
        {
            await Task.Yield();
            var items = GetReviews(shitterId);
            return View(items);
        }


        public IEnumerable<ReviewViewModel> GetReviews(Guid shitterId)
        {
            var reviews = _db.Shitters
                            .Include(x => x.Reviews)
                            .FirstOrDefault(x => x.ID == shitterId)?
                            .Reviews
                            .Select(x => new ReviewViewModel
                                    {
                                        ShitterID = shitterId,
                                        Content = x.Content,
                                        Rating = x.Rating
                                    }
                                    );
            return reviews;
        }
    }
}
