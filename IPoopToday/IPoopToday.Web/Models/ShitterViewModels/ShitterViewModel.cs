﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace IPoopToday.Web.Models.ShitterViewModels
{
    public class ShitterViewModel
    {
        public ShitterViewModel()
        {
        }

        public ShitterViewModel(Guid id, string name,
            double latitude, double longitude)
        {
            ID = id;
            Name = name;
            Latitude = latitude;
            Longitude = longitude;
        }

        [Required]
        public Guid ID { get; set; }

        [RegularExpression("^([0-9]+(\\.[0-9]+)?)$", ErrorMessage = "Incorrent format")]
        public double Latitude { get; set; }

        [RegularExpression("^([0-9]+(\\.[0-9]+)?)$", ErrorMessage = "Incorrent format")]
        public double Longitude { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Name { get; set; }

        [RegularExpression("^([0-9]+(\\.[0-9]+)?)?$", ErrorMessage = "Incorrent format")]
        public decimal Price { get; set; }

        [Required]
        public ShitterTagsViewModel Tags { get; set; }
        public string DebugMessage { get; set; }
        
        public int VotesNo { get; set; }
        public int VotesYes { get; set; }

        public double Opacity
        {
            get
            {
                double ratio = (double)VotesYes / VotesNo;
                if (double.IsNaN(ratio))
                    return 0.5;
                if (double.IsInfinity(ratio))
                    return 1;
                return Math.Max(1, ratio);
            }
        }
        public bool? IsPositiveVote { get; set; }
        public bool IsPaid => Tags.HasFlag(ShitterTagsViewModel.Paid);
        public string AuthorID { get; set; }
        public double AverageRating { get; set; }
        public bool IsNew => ID == Guid.Empty;
        public int ReviewsCount { get; set; }
    }
}
