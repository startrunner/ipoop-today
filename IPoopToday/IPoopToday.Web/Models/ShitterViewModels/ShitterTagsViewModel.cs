﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Models.ShitterViewModels
{
    [Flags]
    public enum ShitterTagsViewModel
    {
        None=0,
        Commercial=1,
        Charity=2,
        Paid=4,
        ForScience=16,
        Unspecified=65536
    }
}
