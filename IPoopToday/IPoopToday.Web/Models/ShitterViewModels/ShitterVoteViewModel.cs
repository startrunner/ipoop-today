﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Models.ShitterViewModels
{
    public class ShitterVoteViewModel
    {
        [Required]
        public Guid ShitterID { get; set; }
        public bool IsPositive { get; set; }
    }
}
