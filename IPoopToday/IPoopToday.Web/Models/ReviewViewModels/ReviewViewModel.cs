﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Models.ReviewViewModels
{
    public class ReviewViewModel
    {
        [Required]
        public Guid ShitterID { get; set; }
        public string Content { get; set; }
        [Range(1,5)]
        public double Rating { get; set; }
    }
}
