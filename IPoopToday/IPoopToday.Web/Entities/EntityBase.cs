﻿using System.ComponentModel.DataAnnotations;

namespace IPoopToday.Web.Entities
{
    public abstract class EntityBase<TID>
    {
        [Required]
        public TID ID { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
