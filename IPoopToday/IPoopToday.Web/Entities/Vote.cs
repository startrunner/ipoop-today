﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Entities
{
    public class Vote:EntityBase<int>
    {
        [ForeignKey(nameof(UserID))]
        public Shitee User { get; set; }

        [ForeignKey(nameof(ShitterID))]
        public Shitter Shitter { get; set; }

        public DateTime Time { get; set; }
        public bool IsPositive { get; set; }

        public string UserID { get; set; }

        public Guid ShitterID { get; set; }
    }
}
