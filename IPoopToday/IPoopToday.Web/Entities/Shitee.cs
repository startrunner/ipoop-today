﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;

namespace IPoopToday.Web.Entities
{
    // Add profile data for application users by adding properties to the Shitee class
    public class Shitee : IdentityUser
    {
        public string FullName { get; set; }
        public virtual List<Vote> VotesCast { get; set; }
        public Shitee()
        {
        }
    }
}
