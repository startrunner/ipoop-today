﻿using System;

namespace IPoopToday.Web.Entities
{
    public class Activity : EntityBase<Guid>
    {
        public ActivityType Type { get; set; }
        public string Comment { get; set; }
        public double OverallPerformance { get; set; }
        public double ProductConsistency { get; set; }
        public double Volume { get; set; }
        public double CleanupDifficulty { get; set; }
        public double Noise { get; set; }
    }
}
