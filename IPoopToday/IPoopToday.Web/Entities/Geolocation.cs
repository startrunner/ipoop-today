﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IPoopToday.Web.Entities
{
    [ComplexType]
    public class Geolocation:EntityBase<int>
    {
        public Geolocation():this(0, 0) { }
        public Geolocation(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        [RegularExpression("^([0-9]+(\\.[0-9]+)?)$")]
        public double Latitude { get; set; }
        [RegularExpression("^([0-9]+(\\.[0-9]+)?)$")]
        public double Longitude { get; set; }
    }
}
