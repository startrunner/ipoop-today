﻿using System.Collections.Generic;

namespace IPoopToday.Web.Entities
{
    public class ActivityType:EntityBase<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<Activity> Activities { get; set; }
    }
}
