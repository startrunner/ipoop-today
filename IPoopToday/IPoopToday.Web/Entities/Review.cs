﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Entities
{
    public class Review:EntityBase<Guid>
    {
        [Range(1, 5)]
        public byte Rating { get; set; }
        public string Content { get; set; }
        [Required]
        public Shitee Author { get; set; }
    }
}
