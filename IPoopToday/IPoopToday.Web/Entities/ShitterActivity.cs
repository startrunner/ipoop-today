﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Entities
{
    public class ShitterActivity:EntityBase<Guid>
    {
        public virtual Shitter Shitter { get; set; }
        public virtual Shitee User { get; set; }
        public DateTime Time { get; set; }
        public double ShitterRating { get; set; }
        public string Comment { get; set; }
    }
}
