﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace IPoopToday.Web.Entities
{
    public class Shitter:EntityBase<Guid>
    {
        [Required]
        public Shitee Author { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public Geolocation Location { get; set; }
        public int Tags { get; set; }
        public virtual List<Vote> Votes { get; set; }
        public  virtual  List<Review> Reviews { get; set; }

        [NotMapped]
        public int VotesYes => Votes?.Count(x => x.IsPositive) ?? 0;
        [NotMapped]
        public int VotesNo => Votes?.Count(x => !x.IsPositive) ?? 0;
    }
}
