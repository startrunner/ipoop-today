﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;
using System.Net;

namespace IPoopToday.Web.Infrastructure
{
    public class DomainRewriterRule : IRule
    {
        public const string Hostname = "iPoop.today";

        public int StatusCode { get; } = (int)HttpStatusCode.MovedPermanently;
        public bool ExcludeLocalhost { get; set; } = true;

        public void ApplyRule(RewriteContext context)
        {
            ;
            HttpRequest request = context.HttpContext.Request;

            string newPath = request.Scheme + "://www." + Hostname + request.PathBase + request.Path + request.QueryString;


            var response = context.HttpContext.Response;
            response.StatusCode = StatusCode;
            response.Headers[HeaderNames.Location] = newPath;
            context.Result = RuleResult.EndResponse; // Do not continue processing the request        
        }
    }
}
