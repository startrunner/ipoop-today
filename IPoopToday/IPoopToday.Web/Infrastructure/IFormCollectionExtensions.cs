﻿using IPoopToday.Web.Models.ShitterViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPoopToday.Web.Infrastructure
{
    public static class IFormCollectionExtensions
    {
        public static ShitterTagsViewModel GetShitterTags(this IFormCollection form)
        {
            ShitterTagsViewModel value = ShitterTagsViewModel.None;

            foreach (var key in form.Keys.Where(x=>x.StartsWith("tag_")))
            {
                string tagName = key.Split('_').Last();
                var current=(ShitterTagsViewModel)Enum.Parse(typeof(ShitterTagsViewModel), tagName);
                bool currentValue = form[key].ToString().ToLower() == "on";

                if(currentValue)
                {
                    value = value | current;
                }
            }

            return value;
        }
    }
}
